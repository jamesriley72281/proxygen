proxygen is a text-based generator of Magic: the Gathering proxies, using oracle text from [Scryfall](https://scryfall.com).

The project is currently live on [proxygen.xyz](https://proxygen.xyz).
