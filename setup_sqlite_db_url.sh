# run by sourcing
# e.g. '. setup_db_url.sh test.sqlite'
# if no arg is given, generates a temporary db file in /tmp
if [ -z "$1" ]
then
	DB_PATH=$(mktemp --suffix=-proxygen.sqlite)
else
	DB_PATH=$1
fi

export DATABASE_URL=sqlite:///$DB_PATH
echo $DATABASE_URL
