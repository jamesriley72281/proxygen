from app import db, Card
import requests
from clint.textui import progress
import io
import json

from data_processing import rawcard_from_json

url = 'https://archive.scryfall.com/json/scryfall-oracle-cards.json'

f = io.BytesIO()
r = requests.get(url, stream=True)
total_length = int(r.headers.get('content-length'))
chunks = []
for chunk in progress.bar(r.iter_content(chunk_size=1024, decode_unicode=True), expected_size=(total_length/1024) + 1, label='Downloading JSON ', every=100):
    if chunk:
        f.write(chunk)
        f.flush()

cards = json.loads(f.getvalue())

new_cards = []
for card in progress.bar(cards, label='Processing JSON ', every=100):
    raw = rawcard_from_json(card)
    if raw is not None:
        c = Card(id=card['oracle_id'], name=raw.name, html=raw.to_html())
        new_cards.append(c)

def get_uuid(card):
    return card.id
old_cards = Card.query.all()
old_cards_map = { c.id : c for c in old_cards }
assert(len(old_cards) <= len(new_cards))

add_list = []
update_list = []
for new in progress.bar(new_cards, label='Adding cards to DB ', every=100):
    old = old_cards_map.get(new.id)
    if old is None:
        add_list.append(new)
    else :
        update_list.append((old, new))

db.session.add_all(add_list)

update_count = 0
for (old, new) in progress.bar(update_list, label='Updating card entries ', every=100):
    assert(old.id == new.id)
    need_update = False
    if old.name != new.name:
        need_update = True
    if old.html != new.html:
        need_update = True
    if need_update:
        old.name = new.name
        old.html = new.html
        update_count += 1

if update_count > 0 or len(add_list) > 0:
    db.session.commit()

print('DB update successful')
print('{} cards added, {} updated'.format(len(add_list), update_count))
