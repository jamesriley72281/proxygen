from flask import Flask, request, render_template, Markup
from flask_sqlalchemy import SQLAlchemy
from data_processing import sanitize_cardname
from flask_weasyprint import HTML, render_pdf
import os
import re

DATABASE_URL = os.environ['DATABASE_URL']

MAX_CARDS = 300
BASE_RE = '(\d+)?x?\s*(\D*?)\s*$'
BASE_RE_PROG = re.compile(BASE_RE)
SPLIT_RE = '(\D+?)\s*\/+.*$'
SPLIT_RE_PROG = re.compile(SPLIT_RE)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
#heroku = Heroku(app)
db = SQLAlchemy(app)

class RecordCards(db.Model):
    card_id = db.Column(db.String(36), db.ForeignKey(
        'card.id'), primary_key=True)
    record_id = db.Column(db.Integer, db.ForeignKey(
        'record.id'), primary_key=True)
    amount = db.Column(db.Integer, nullable=False)
    card = db.relationship('Card')

class Record(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime(timezone=True),
                     server_default=db.func.now(), nullable=False)
    cards = db.relationship('RecordCards')

class Card(db.Model):
    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.Text, nullable=False)
    html = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return '<Card {}>'.format(self.name)

@app.route('/')
def proxygen():
    make_pdf = request.args.get('make_pdf')

    decklist = request.args.get('decklist')

    if decklist is None:
        return app.send_static_file('index.html')

    raw_in = decklist.splitlines()
    amounts = {}
    cards_names = []
    for raw in raw_in:
        raw = raw.strip()
        m = BASE_RE_PROG.match(raw)
        if m is None:
            continue
        amount = m.group(1)
        if amount is None:
            amount = 1
        else:
            amount = int(amount)
        name = m.group(2)
        m = SPLIT_RE_PROG.match(name)
        if m is not None:
            name = m.group(1)
        name = sanitize_cardname(name)
        if len(name) > 0:
            cards_names.extend([name] * amount)
            amounts[name] = amounts.get(name, 0) + amount

    num_cards = sum(amounts.values())
    if num_cards > MAX_CARDS:
        return 'Error: At most {} cards per request'.format(MAX_CARDS), 400

    c_list = Card.query.filter(Card.name.in_(amounts.keys())).all()
    error = set(amounts.keys()) - set([c.name for c in c_list])
    if len(error) > 0:
        return 'Error: Unrecognized cards {}'.format(error), 400

    name_card_map = {}
    for c in c_list:
        name_card_map[c.name] = c

    cards = []
    for c_name in cards_names:
        cards.append(name_card_map[c_name])

    record = Record()
    for c in c_list:
        amount = amounts[c.name]
        a = RecordCards(amount=amount)
        a.card = c;
        record.cards.append(a)
    db.session.add(record)
    db.session.commit()

    safe_html_list = [Markup(card.html) for card in cards]
    final_html = render_template('results.html', safe_html_list=safe_html_list)

    if make_pdf == 'yes':
        return render_pdf(HTML(string=final_html))
    else:
        return final_html
